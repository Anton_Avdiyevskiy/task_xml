package com.xmlparsing;


import com.model.Periodical;
import java.io.File;
import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class SaxParser extends DefaultHandler implements Parser {
    @Override
    public  List<Periodical> parseDoc() {
        SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
        List<Periodical> perList = null;
        try {
            SAXParser saxParser = saxParserFactory.newSAXParser();
            CustomHandler customHandler = new CustomHandler();
            saxParser.parse(new File("src/periodical.xml"), customHandler);
            perList = customHandler.getPerList();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        perList.sort(Comparator.comparing(Periodical::getPages));
        for (Periodical periodical : perList) {
            System.out.println(periodical);
        }
        return perList;
    }
}
